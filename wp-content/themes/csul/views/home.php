<?php
/**
 * Template Name: Home
 *
 * Template for displaying Home Page
 *
 * @package AgenciaOpen
 */

get_header();
$container = get_theme_mod( 'AgenciaOpen_container_type' );
?>



					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'loop-templates/content', 'page' ); ?>

						<?php
						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :

							comments_template();

						endif;
						?>

					<?php endwhile; // end of the loop. ?>

				
<?php get_footer(); ?>
