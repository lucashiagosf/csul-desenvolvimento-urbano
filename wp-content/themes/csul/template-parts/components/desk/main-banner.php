<?php get_template_part('template-parts/components/desk/shared/styles'); ?>
<?php // get_template_part('template-parts/components/desk/shared/nav'); ?>

<?php $banner = get_field('banner_home'); ?>

<header id="masthead" class="site-header bg-is-home">
    <div class="banner_content" style="background-image:url(<?php echo $banner['url']; ?>)">
        <div class="color_blue"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="text-uppercase">Para quem quer um novo conceito de viver bem</h1>
                    <sub>Um verdadeiro bairro estruturado<br>para você encontrar o seu lugar.</sub>
                    <a href="" class="btn btn_border_g white" title="Conheça a Masterplan ">Conheça a Masterplan </a>
                </div>
            </div>
        </div>
    </div>
</header>