<?php
/**
 * Partial template for content in page.php
 *
 * @package AgenciaOpen
 */

?>
<section id="about">
    <div class="filter_b"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="family">
                    <div class="circle_clear"></div>
                </div>
                <div class="relative text_about">
                    <?php echo the_field('text') ?>
					<a href="" class="btn btn_border_g-full white" title="Conheça a CSUL">Conheça a CSUL</a>
					<div class="retang"></div>
					<div class="quadra"></div>
                </div>
            </div>
            <div class="col-md-5 offset-md-1">
                <div class="img_pan">
                    <div class="color_blue"></div>
                </div>
                
				<div class="retang_ligh"></div>
				<div class="text_sec">
					<h2>Nesse contexto, nasce o projeto <span>Masterplan</span></h2>
					<p>O projeto, concebido por Jaime Lerner segue os preceitos do novo urbanismo, onde os espaços das cidades priorizam a diversidade e as pessoas: moradia e trabalho, lazer e cultura, como forma de redução dos deslocamentos e melhoria da qualidade de vida.</p>
					<a href="" class="btn btn_border_g white" title="Conheça a Masterplan">Conheça a Masterplan</a>
				</div>

            </div>
        </div>
    </div>
</section>

<!--<footer class="entry-footer">

		<?php edit_post_link( __( 'Edit', 'AgenciaOpen' ), '<span class="edit-link">', '</span>' ); ?>

	</footer> .entry-footer -->